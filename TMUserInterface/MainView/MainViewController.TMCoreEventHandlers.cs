﻿using System;

using TMCore.EventArguments;
using TMUserInterface.StatusMessages;

namespace TMUserInterface
{
    partial class MainViewController
    {
		public void OnReset(Object sender, TapeResetEventArgs e)
		{
			//_setTMData(_tm.StepsExecuted, _tm.CurrentState.Name, "------", _tm.GetTapeAsString(5, 5));
			_ClearLog();
		}

		public void OnStepFinished(Object sender, StepFinishedEventArgs e)
		{
            _UpdateTMStateData();
			_PrintLineToLog(String.Format("{0})\t{1}\t\t{2}\t\t{3}",
											e.StepOrdinal,
											e.ExecutedStatement,
											e.ExecutedStatement.NewState,
											e.TapeRepresentation));

            if (_stepsLimit != 0 && _tm.StepsExecuted >= _stepsLimit)
            {
				OnPause();
                _PrintStatus(SuccessStatusMessages.STEPS_LIMIT_REACHED, StatusType.Success);
            }

		}

        public void OnFinishedRunning(Object sender, EventArgs e)
        {
            _simulatorState = TMSimulatorState.Finished;
        }
    }
}
