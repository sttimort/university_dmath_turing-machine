using System;

using AppKit;

using TMCore.Interfaces;

namespace TMUserInterface
{
	public partial class TMStatementAddButton : NSButton
	{
		public TMStatementAddButton (IntPtr handle) : base (handle)
		{
		}

		public ITMState OldState { get; set; }
		public char OldSymbol { get; set; }
	}
}
