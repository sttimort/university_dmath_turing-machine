﻿using System;
using System.Collections.Generic;

using TMCore.Exceptions;
using TMCore.Interfaces;

namespace TMUserInterface
{
    public class TMSymbols
    {
        List<char> _symbols;

        public TMSymbols()
        {
            _symbols = new List<char>();
        }

        public TMSymbols(ITMProgram program, char zeroCharacter)
        {
            _symbols = new List<char>() {
                zeroCharacter
            };

            foreach (var stmt in program.Statements)
            {
                if (!_symbols.Exists((sym) => sym == stmt.OldSymbol) &&
                    stmt.OldSymbol != zeroCharacter)
                    _symbols.Add(stmt.OldSymbol);

                if (!_symbols.Exists((sym) => sym == stmt.NewSymbol) &&
                    stmt.NewSymbol != zeroCharacter)
                    _symbols.Add(stmt.NewSymbol);
            }

            _symbols.Sort((s1, s2) => {
                if (s1 == zeroCharacter || s1 > s2)
                    return 1;
                else if (s1 < s2)
                    return -1;
                else
                    return 0;
            });
        }

        public int Count {
            get { return _symbols.Count; }
        }

        public char this[int i] => _symbols[i];

        public void Add(char symbol)
		{
            if (_symbols.Count >= 4)
				throw new TMachineException("You can't add more that 4 symbols");

            if (_symbols.Exists(c => { return (c == symbol); }))
				throw new TMachineException("Symbol already exists");

            _symbols.Add(symbol);
            OnAlphabetChanged(new AlphabetChangedEventArgs() {
                added = new char[] { symbol },
                removed = null
            });
		}

        public void Remove(char symbol)
        {
        	if (_symbols.Count == 0 || !_symbols.Exists(c => { return (c == symbol); }))
				throw new TMachineException("Can't delete non-existant symbol");

            _symbols.Remove(symbol);
			OnAlphabetChanged(new AlphabetChangedEventArgs() {
                added = null,
                removed = new char[] { symbol }
			});
        }

        public event EventHandler<AlphabetChangedEventArgs> AlphabetChanged;

        public class AlphabetChangedEventArgs : EventArgs
        {
            public char[] added;
            public char[] removed;
        }

        void OnAlphabetChanged(AlphabetChangedEventArgs e)
        {
            var handler = AlphabetChanged;
            if (handler != null)
                handler.Invoke(this, e);
        }
    }
}
