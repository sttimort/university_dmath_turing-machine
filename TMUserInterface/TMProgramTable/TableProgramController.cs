﻿using System;
using System.Collections.Generic;

using AppKit;
using Foundation;


using TMCore.Exceptions;

namespace TMUserInterface.ProgramTablee
{
    public class ProgramTableController
    {
        NSTableView _tv;

        public ProgramTableController(NSTableView tv)
        {
            _tv = tv;
            foreach (var col in _tv.TableColumns())
                _tv.RemoveColumn(col);

            var col1 = new NSTableColumn("statesCol")
            {
                Title = "States",
                Width = 75,
                Editable = false,
                ResizingMask = NSTableColumnResizing.None
            };
            _tv.AddColumn(col1);
        }

        public void AddNewColumn(char symbol)
        {
            var col = new NSTableColumn(symbol.ToString())
            {
                Title = symbol.ToString(),
                Editable = false,
                Width = 75,
                ResizingMask = NSTableColumnResizing.None
            };
            _tv.AddColumn(col);
        }

        public void RemoveColumn(char symbol)
        {
            if (!DoesColumnlExists(symbol))
                throw new TMachineException("Specified column doesn't exist");
            
            _tv.RemoveColumn(_tv.FindTableColumn(new NSString(symbol.ToString())));
        }

        bool DoesColumnlExists(char symbol)
        {
            foreach (var col in _tv.TableColumns())
            {
                if (col.Title == symbol.ToString())
                    return true;
            }
            return false;
        }
    }
}
