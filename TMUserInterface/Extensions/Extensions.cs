﻿using AppKit;

namespace TMUserInterface.Extensions
{
    public static class Extensions
    {
        public static string ToFriendlyString(this MainViewController.TMSimulatorState state)
        {
            switch (state)
            {
                case MainViewController.TMSimulatorState.NotReady:
                    return "Not Ready";
                case MainViewController.TMSimulatorState.Ready:
                    return "Ready";
                case MainViewController.TMSimulatorState.Executing:
                    return "Executing";
                case MainViewController.TMSimulatorState.Paused:
                    return "Paused";
                case MainViewController.TMSimulatorState.Finished:
                    return "Finished";
                default:
                    return state.ToString();
            }
        }

        public static NSView Find(this NSView[] views, string identifier)
        {
            foreach (var view in views)
                if (view.Identifier == identifier)
                    return view;

            return null;
        }

        public static void DisableStatements(this NSTableView tableView)
        {
            int cc = (int)tableView.ColumnCount;
            int rc = (int)tableView.RowCount;

			for (int i = 0; i < rc; i++)
				for (int j = 1; j < cc; j++)
				{
					var cell = tableView.GetView(j, i, true);

					var v = cell.Subviews.Find("NewStatementButton") as NSButton;
                    if (v != null) v.Enabled = false;

					v = cell.Subviews.Find("NewStatePopUp") as NSButton;
                    if (v != null) v.Enabled = false;
					v = cell.Subviews.Find("NewSymbolPopUp") as NSButton;
                    if (v != null) v.Enabled = false;
					v = cell.Subviews.Find("DirectionPopUp") as NSButton;
                    if (v != null) v.Enabled = false;
				}
        }

		public static void EnableStatements(this NSTableView tableView)
		{
			int cc = (int)tableView.ColumnCount;
			int rc = (int)tableView.RowCount;

			for (int i = 0; i < rc; i++)
				for (int j = 1; j < cc; j++)
				{
					var cell = tableView.GetView(j, i, true);

                    var v = cell.Subviews.Find("NewStatementButton") as NSButton;
                    if (v != null) v.Enabled = true;

					v = cell.Subviews.Find("NewStatePopUp") as NSButton;
					if (v != null) v.Enabled = true;
					v = cell.Subviews.Find("NewSymbolPopUp") as NSButton;
					if (v != null) v.Enabled = true;
					v = cell.Subviews.Find("DirectionPopUp") as NSButton;
					if (v != null) v.Enabled = true;
				}
		}
    }
}
