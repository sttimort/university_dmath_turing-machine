﻿﻿using System;
using System.Collections.Generic;

using AppKit;

using TMCore;
using TMCore.Interfaces;
using TMCore.Exceptions;

namespace TMUserInterface
{
    public class TMStatesList : NSTableViewDataSource
    {
        List<ITMState> _states;

        public TMStatesList()
        {
            _states = new List<ITMState>();
        }

        public TMStatesList(ITMProgram program)
        {
            _states = new List<ITMState>() {
                program.InitialState
            };

            foreach (var stmt in program.Statements)
            {
                if (!_states.Exists((s) => { return s == stmt.OldState; }) &&
                    stmt.OldState != program.FinalState &&
                    stmt.OldState != program.InitialState)
                    _states.Add(stmt.OldState);

                if (!_states.Exists((s) => { return s == stmt.NewState; }) &&
                    stmt.NewState != program.FinalState &&
                    stmt.NewState != program.InitialState)
                    _states.Add(stmt.NewState);
            }

            _states.Sort((s1, s2) => string.Compare(s1.Name, s2.Name));
        }

        public ITMState this[int i] => _states[i];

        public ITMState this[string name] {
            get {
                var state = _states.Find((s) => s.Name == name);

                return state;
            }
        }

        public int Count {
            get { return _states.Count; }
        }


        public void AddWithName(string name)
        {
            if (_states.Exists((s) => s.Name == name))
                throw new TMachineException("State with given name alreadt exists");

            Add(new TMState(name));
        }

        public void Add(ITMState state)
        {
            if (_states.Count >= 4)
                throw new TMachineException("You can't add more that 4 states.");

            if (_states.Exists(c => { return (c == state); }))
				throw new TMachineException("State already exists");

            _states.Add(state);
            StatesListChanged(this, EventArgs.Empty);
        }

        public void RemoveStateWithName(string stateName)
        {
            if (_states.Count == 0)
                throw new TMachineException("States list is empty, remove is not allowed");

            int idx = _states.FindIndex((s) => s.Name == stateName);

            if (idx == -1)
                throw new TMachineException("States with given name doesn't exist");

            _states.RemoveAt(idx);
            StatesListChanged(this, EventArgs.Empty);
        }

        public override nint GetRowCount(NSTableView tableView)
        {
            return _states.Count;
        }


		public event EventHandler StatesListChanged;
    }
}
