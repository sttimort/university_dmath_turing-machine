﻿namespace TMUserInterface.StatusMessages
{
    public static class ErrorStatusMessages
    {
        public static readonly string INVALID_INPUT_STRING = "Whitespace characters aren't allowed in input string";
        public static readonly string INVALID_INITIAL_HEAD_POSITION = "Invalid initial head position";
        public static readonly string INITIAL_HEAD_POSITION_IS_NVI = "Initial head position is not valid integer";
        public static readonly string INITIAL_HEAD_POSITION_TOO_LARGE = "Initial head position is too large";
        public static readonly string INVALIT_STEPS_LIMIT = "Steps limit must be positive integer or zero (unlimited)";
        public static readonly string INVALID_SYMBOL = "To add new symbol type exactly ONE non-whitesapce symbol in the field";
        public static readonly string CANT_DELETE_ZERO_SYMBOL = "You cant delete zero character";
    }
}
