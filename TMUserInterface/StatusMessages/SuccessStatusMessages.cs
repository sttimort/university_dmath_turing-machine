﻿namespace TMUserInterface.StatusMessages
{
    static public class SuccessStatusMessages
    {
        public static readonly string TMS_LOADED = "Turing Machine Simulator has started successfuly";
        public static readonly string PROGRAMM_FINISHED = "Program finished executing successfuly";

        public static readonly string INPUT_STRING_CLEARED = "Input string cleared successfuly";
        public static readonly string INITIAL_HEAD_POSITION_SET_TO = "Initial head positin set to ";

        public static readonly string STEPS_LIMIT_REACHED = "Steps limit reached";

        public static readonly string STEPS_LIMIT_SET_TO_ZERO = "Steps limit set to zero (unlimited)";

        public static string InputStringLoadedWithInitialHeadPosition(
            string inputString, int initialHeadPosition)
        {
            return string.Format("Input string \"{0}\" loaded with initial head position {1}",
                                 inputString, initialHeadPosition);
        }

		public static string StepsLimitSetToNumber(int stepsLimit)
		{
            return string.Format("Steps limit set to {0}", stepsLimit);
		}

		public static string FinalStateSetTo(string finalStateName)
		{
            return string.Format("Final state name is now {0}", finalStateName);
		}

		public static string ZeroSymbolSetTo(char symbol)
		{
            return string.Format("Zero symbol set to {0}", symbol);
		}

		public static string NewSymbolAdded(char symbol)
		{
			return string.Format("Symbol {0} added", symbol);
		}

		public static string SymbolDeleted(char symbol)
		{
			return string.Format("Symbol {0} deleted", symbol);
		}

		public static string StateAdded(string stateName)
		{
            return string.Format("State {0} added", stateName);
		}

        public static string StateDeleted(string stateName)
        {
            return string.Format("State {0} deleted", stateName);
        }
    }
}
