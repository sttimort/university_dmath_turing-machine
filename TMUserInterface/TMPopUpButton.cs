using System;

using AppKit;

using TMCore.Interfaces;

namespace TMUserInterface
{
	public partial class TMPopUpButton : NSPopUpButton
	{
		public TMPopUpButton (IntPtr handle) : base (handle)
		{
		}

        public ITMState OldState { get; set; }
        public char OldSymbol { get; set; }
	}
}
