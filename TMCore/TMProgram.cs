﻿using System;
using System.Collections.Generic;
using System.Linq;

using TMCore.Interfaces;
using TMCore.Exceptions;

namespace TMCore
{
    public class TMProgram : ITMProgram
    {
        ITMState _initialState;
        ITMState _finalState;
        List<TMState> _states;
        List<ITMStatement> _statements;


        public TMProgram(
            ITMState initialState = null, ITMState finalState = null, IList<ITMStatement> statements = null)
        {
            _initialState = initialState != null ? initialState : new TMState("init");
            _finalState = finalState != null ? finalState : new TMState("final");

            if (statements == null)
                _statements = new List<ITMStatement>();
            else
                _statements = new List<ITMStatement>(statements);
        }



        public ITMState InitialState
        {
            get { return _initialState; }
            set {
                if (value == _finalState)
                    throw new TMachineException("Initial and final states has to be different");
                
                _initialState = value; 
                OnInitialStateChnged();
            }
        }

        public ITMState FinalState
        {
            get { return _finalState; }
			set {
                if (value == _initialState)
					throw new TMachineException("Initial and final states has to be different");

                _finalState = value;
			}
        }

        public IList<ITMStatement> Statements
        {
            get { return _statements; }
        }

        public bool IsReady {
            get {
                if (_initialState == null || _finalState == null ||
                    _statements.Count < 1)
                    return false;
                
                return true;
            }
        }


        public void AddState(TMState state)
        {
            if (_states.Find((s) => s.Name == state.Name) != null || FinalState.Name == state.Name)
                throw new TMachineException(string.Format("State with name {0} already exists", state.Name));

            _states.Add(state);
        }


        public ITMStatement StatementForOldStateAndSymbol(ITMState oldState, char oldSymbol)
        {
            if (_statements == null)
                return null;
            
            IEnumerable<ITMStatement> queryResults =
                from stmt in _statements
                    where stmt.OldState == oldState && stmt.OldSymbol == oldSymbol
                    select stmt;

            return queryResults.FirstOrDefault();
        }

        public ITMProgram GetCopy()
        {
            return new TMProgram(_initialState, _finalState, _statements);
        }

        public event EventHandler InitialStateChanged;

        void OnInitialStateChnged()
        {
            var handler = InitialStateChanged;
            if (handler != null)
                handler.Invoke(this, EventArgs.Empty);
        }
    }

}
