﻿using System.Linq;
using System.Text;
using System.Collections.Generic;

using TMCore.Interfaces;

namespace TMCore
{
    public class Tape : ITape
    {
        public const char DEFAULT_ZERO_CHARACTER = '*';
        public const int DEFAULT_GROWTH_SIZE = 50;
        public const int INITIAL_SIZE = 10;

        char _zeroCharacter = DEFAULT_ZERO_CHARACTER;
        int _pointer;
		char[] _cells;

        int _interestAreaStartPointer;
        int _interestAreaEndPointer;

        public Tape(char zeroCharacter = DEFAULT_ZERO_CHARACTER)
        {
            _zeroCharacter = zeroCharacter;
            ResetWithZeroCharacters();
        }

        public Tape(string inputString, char zeroCharacter = DEFAULT_ZERO_CHARACTER)
        {
            _zeroCharacter = zeroCharacter;

            if (inputString.Length == 0)
                ResetWithZeroCharacters();
            else
                ResetWithInputString(inputString);
        }


        #region Properties 

        public char ZeroCharacter {
            get { return _zeroCharacter; }
            set { _zeroCharacter = value; }
        }

        public char CurrentSymbol {
            get { return _cells[_pointer]; }
        }

        public ITapeRepresentation Representation {
            get { return _CreateRepresentation(); }
        }
        #endregion

        ITapeRepresentation _CreateRepresentation()
        {
            var cells = new List<char> {
                _zeroCharacter
            };

            for (int i = _interestAreaStartPointer; i <= _interestAreaEndPointer; i++)
            {
                cells.Add(_cells[i]);
            }
                     
            cells.Add(_zeroCharacter);

            return new TapeRepresentation(_pointer-_interestAreaStartPointer+1, cells);
        }

		void _GrowRight(int size = DEFAULT_GROWTH_SIZE)
		{
			char[] tmp = (char[])_cells.Clone();
			_cells = new char[tmp.Length + size];

			tmp.CopyTo(_cells, 0);
			_FillWithZeroCharacters(_cells.Length - size, size);
		}

		void _GrowLeft(int size = DEFAULT_GROWTH_SIZE)
		{
			char[] tmp = (char[])_cells.Clone();
			_cells = new char[tmp.Length + size];

			tmp.CopyTo(_cells, size);
			_FillWithZeroCharacters(0, size);
			_pointer = size; // reindex the pointer
            _interestAreaEndPointer += size;
            _interestAreaStartPointer += size;
		}

		void _FillWithZeroCharacters(int index, int count)
		{
			for (int i = index; i < index + count; i++)
			{
				_cells[i] = _zeroCharacter;
			}
		}

        public void MoveRight()
        {
            if (_pointer >= (_cells.Length - 1))
                _GrowRight();

            _pointer++;

            if (_pointer > _interestAreaEndPointer)
                _interestAreaEndPointer = _pointer;
        }

        public void MoveLeft()
        {
            if (_pointer == 0)
                _GrowLeft();

            _pointer--;

            if (_pointer < _interestAreaStartPointer)
                _interestAreaStartPointer = _pointer;
        }

        
        // Fills the tape with zero characters in the specified range

        public void ResetWithZeroCharacters()
        {
            _cells = new char[INITIAL_SIZE];
            _FillWithZeroCharacters(0, INITIAL_SIZE);

            _pointer = 0;
            _interestAreaStartPointer = 0;
            _interestAreaEndPointer = INITIAL_SIZE - 1;
        }

        public void ResetWithInputString(string inputString, int initialHeadPosition = 0)
        {
            if (inputString.Length == 0)
                ResetWithZeroCharacters();
            else
            {
                _cells = new char[inputString.Length];
				inputString.ToCharArray().CopyTo(_cells, 0);

				_pointer = initialHeadPosition;
				_interestAreaStartPointer = 0;
                _interestAreaEndPointer = _cells.Length - 1;
            }

        }

        public void Write(char newSymbol)
        {
            _cells[_pointer] = newSymbol;
        }

        public string GetCellsAsString(int fromLeft, int fromRight)
        {
            var result = new StringBuilder(fromLeft + fromRight + 1);

            if (fromLeft > _pointer)
            {
                // If there are not enough symbols on the left, fill with blanks
                result.Append(new string(DEFAULT_ZERO_CHARACTER, (fromLeft - _pointer)));
                // Now append the symbols on the left of the head
                result.Append(_cells.Take(_pointer).ToArray());
            }
            else
                // Read the required number of symbols from the left
                result.Append(_cells.Skip(_pointer - fromLeft).Take(fromLeft).ToArray());

            result.Append(_cells[_pointer]);

            if (fromRight > (_cells.Length - _pointer - 1))
            {
                // Not enough symbols on the right side, so first read all the available symbols
                result.Append(_cells.Skip(_pointer+1).Take(_cells.Length - _pointer - 1).ToArray());
                // ... and then fill with blanks
                result.Append(new string(DEFAULT_ZERO_CHARACTER,
                                         (fromRight - (_cells.Length - _pointer - 1))));
            }
            else
                // Just read the symbols on the right
                result.Append(_cells.Skip(_pointer+1).Take(fromRight).ToArray());


            return result.ToString();
        }

    }
}
