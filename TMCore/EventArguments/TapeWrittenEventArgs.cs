﻿using System;

namespace TMCore.EventArguments
{
    public class TapeWrittenEventArgs : EventArgs
    {
        public char NewSymbol;

        public TapeWrittenEventArgs(char NewSymbol)
        {
            this.NewSymbol = NewSymbol;
        }
    }
}
