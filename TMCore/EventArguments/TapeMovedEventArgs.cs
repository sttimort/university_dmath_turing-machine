﻿using System;

using TMCore.Enums;

namespace TMCore.EventArguments
{
	public class TapeMovedEventArgs : EventArgs
	{
        public StepDirection Direction;

        public TapeMovedEventArgs(StepDirection Direction)
        {
            this.Direction = Direction;
        }
	}
}
