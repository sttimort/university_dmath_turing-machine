﻿using System;

using TMCore.Interfaces;

namespace TMCore.EventArguments
{
    public class TapeResetEventArgs : EventArgs
    {
        public ITapeRepresentation TapeRepresentation;

        public TapeResetEventArgs(ITapeRepresentation TapeRepresentation)
        {
            this.TapeRepresentation = TapeRepresentation;
        }
    }
}
