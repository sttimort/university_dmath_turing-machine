﻿using System;
using System.Threading;
using System.Threading.Tasks;

using TMCore.Interfaces;
using TMCore.Exceptions;
using TMCore.EventArguments;
using TMCore.Enums;

namespace TMCore
{
    public class Machine
    {
        enum MachineState {Running, Paused, Finished}


        int _stepsExecuted;

		ITMProgram _program;

        ITMState _currentState;

        ITape _tape;

        bool _isRunning;
        bool _cancelled;

        int _delay;


        #region Properties

        public int StepsExecuted { 
            get { return _stepsExecuted; } 
        }

        public ITMProgram Program {
            get { return _program.GetCopy(); }
            set {
                if (_isRunning)
                {
                    throw new TMachineException("You can't set new programm " +
                                                "while machine is running.");
                }

                _program.InitialStateChanged -= OnInitialStateChanged;
                _program = value;
                _program.InitialStateChanged += OnInitialStateChanged;
            }
        }

		public ITMProgram EditableProgram { 
            get {
                if (_isRunning)
				{
					throw new TMachineException("You can't modify programm " +
												"while machine is running.");
				}

                return _program; 
            } 
        }

        public ITMState CurrentState { 
            get { return _currentState; } 
        }

        public ITMStatement NextStatement { 
            get { return _program.StatementForOldStateAndSymbol(_currentState, _tape.CurrentSymbol); }
        }


        public ITapeRepresentation TapeRepresentation {
            get { return _tape.Representation; }
        }

        public char ZeroCharacter { 
            get { return _tape.ZeroCharacter; } 
            set {
				if (_isRunning)
				{
					throw new TMachineException("You can't change zero character " +
												"while machine is running.");
				}

                _tape.ZeroCharacter = value;
            }
        }

        public int Delay {
            get { return _delay; }
            set { _delay = value; }
        }
        #endregion


        public Machine(ITape tape, ITMProgram program, string inputString = "", int startFrom = 0)
        {
            _tape = tape;
            _tape.ResetWithInputString(inputString, startFrom);

            _program = program;
            _program.InitialStateChanged += OnInitialStateChanged;

            _delay = 250;

            _currentState = _program.InitialState;
            _stepsExecuted = 0;
            _isRunning = false;
            _cancelled = false;
        }


        public void ResetWithInputString(string inputString, int startFrom = 0)
        {
            if (startFrom < 0)
                throw new TMachineException("Start index must be positive or 0");

            if (_isRunning)
                throw new TMachineException("You can't load new input while machine is running");

            _isRunning = false;
            _cancelled = false;
            _currentState = _program.InitialState;
            _stepsExecuted = 0;
            if (inputString != null)
                _tape.ResetWithInputString(inputString, startFrom);

            OnTapeReset(new TapeResetEventArgs(_tape.Representation));
        }

	    public string GetTapeAsString(int fromLeft, int fromRight)
		{
			return _tape.GetCellsAsString(fromLeft, fromRight);
		}

        public async Task Run()
		{
            if (_program == null || !_program.IsReady)
                throw new TMachineException("Program was not loaded or is incomplete");

			if (_currentState == _program.FinalState)
				throw new TMachineException("Machine finished executing");

            _isRunning = true;
            _cancelled = false;
			while (_currentState != _program.FinalState && !_cancelled)
			{
                await Task.Delay(_delay);
				Step();
			}
            _isRunning = false;
		}

		public void Step()
		{
			if (_currentState == _program.FinalState)
                throw new TMachineException("Machine finished executing");
			
            var stmt = _program.StatementForOldStateAndSymbol(_currentState, _tape.CurrentSymbol);

            // No rule for machine's current state and symbol
            if ( stmt == null)
            {
                _isRunning = false;
                throw new TMachineException("No rule for machine's current state and symbol");
            }

            // Execute statement
            _currentState = stmt.NewState;
            OnStateChanged(new StateChangedEventArgs(_currentState));
			
            _tape.Write(stmt.NewSymbol);
            OnTapeWritten(new TapeWrittenEventArgs(stmt.NewSymbol));
			
            if (stmt.Direction == StepDirection.Right)
				_tape.MoveRight();
            else if (stmt.Direction == StepDirection.Left)
				_tape.MoveLeft();
			
            OnTapeMoved(new TapeMovedEventArgs(stmt.Direction));

            _stepsExecuted++;
            OnStepFinished(new StepFinishedEventArgs(_stepsExecuted, stmt, _tape.Representation));

            if (_currentState == _program.FinalState)
                OnFinishedRunning();
		}

        public void Pause()
        {
            _cancelled = true;
        }

        public void Stop()
        {
            Pause();
            _tape.ResetWithZeroCharacters();
            _currentState = _program.InitialState;
            _stepsExecuted = 0;
            _isRunning = false;
            _cancelled = false;

            OnTapeReset(new TapeResetEventArgs(_tape.Representation));
        }

        public void Clear()
        {
            _tape.ResetWithZeroCharacters();
            _program.Statements.Clear();
        }


        void OnInitialStateChanged(object sender, EventArgs e)
        {
            if (_stepsExecuted == 0)
                _currentState = _program.InitialState;
        }

		public event EventHandler<StateChangedEventArgs> StateChanged;
        public event EventHandler<TapeWrittenEventArgs> TapeWritten;
		public event EventHandler<TapeMovedEventArgs> TapeMoved;
        public event EventHandler<TapeResetEventArgs> TapeReset;
		public event EventHandler<StepFinishedEventArgs> StepFinished;
		public event EventHandler FinishedRunning;

        void OnStateChanged(StateChangedEventArgs e)
        {
            var handler = StateChanged;
            if (handler != null)
                handler.Invoke(this, e);
        }

		void OnTapeWritten(TapeWrittenEventArgs e)
		{
            var handler = TapeWritten;
			if (handler != null)
				handler.Invoke(this, e);
		}

        void OnTapeMoved(TapeMovedEventArgs e)
		{
            var handler = TapeMoved;
			if (handler != null)
				handler.Invoke(this, e);
		}

		void OnTapeReset(TapeResetEventArgs e)
		{
			var handler = TapeReset;
			if (handler != null)
				handler.Invoke(this, e);
		}

        void OnStepFinished(StepFinishedEventArgs e)
		{
            var handler = StepFinished;
			if (handler != null)
				handler.Invoke(this, e);
		}

        void OnFinishedRunning()
        {
            var handler = FinishedRunning;
            if (handler != null)
                handler.Invoke(this, EventArgs.Empty);
        }
    }

}
