﻿using System.Text.RegularExpressions;

using TMCore.Interfaces;
using TMCore.Exceptions;

namespace TMCore
{
    public class TMState : ITMState
    {
        string _name;
        public TMState(string name)
        {
            Name = name;
        }

        public string Name { 
            get { return _name; }
            set {
                if (!nameValid(value))
                    throw new TMachineException("Empty state name");

                _name = value;
            } 
        }

        bool nameValid(string name)
        {
            return Regex.IsMatch(name, @"\w+");
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
