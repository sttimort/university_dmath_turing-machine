﻿using System.Collections.Generic;

namespace TMCore.Interfaces
{
    public interface ITapeRepresentation
    {
        int HeadPosition { get; }

        IList<char> Cells { get; }
    }
}
