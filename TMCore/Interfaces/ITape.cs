﻿namespace TMCore.Interfaces
{
    public interface ITape
    {
        char ZeroCharacter { get; set; }

        char CurrentSymbol { get; }

        ITapeRepresentation Representation { get; }

        void MoveRight();
        void MoveLeft();

        void ResetWithZeroCharacters();
        void ResetWithInputString(string InputString, int startFrom = 1);
        void Write(char NewSymbol);

        string GetCellsAsString(int fromLeft, int fromRight);
    }
}
