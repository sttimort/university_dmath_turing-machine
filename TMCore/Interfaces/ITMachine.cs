﻿using System;

using TMCore.EventArguments;

namespace TMCore.Interfaces
{
    public interface ITMachine
    {
        int StepsExecuted { get; }

        ITMProgram Program { get; set; }
        ITMProgram EditableProgram { get; }

        ITMState CurrentState { get; }

        ITMStatement NextStatement { get; }

        ITapeRepresentation TapeRepresentation { get; }
        char ZeroCharacter { get; set; }


		void LoadInputString(string inputString, int startFrom);
        string GetTapeAsString(int fromLeft, int fromRight);

        void Run();
        void Step();
        void Pause();
        void Stop();

        void Clear();

        event EventHandler<StateChangedEventArgs> StateChanged;
        event EventHandler<TapeWrittenEventArgs> TapeWritten;
        event EventHandler<TapeMovedEventArgs> TapeMoved;
        event EventHandler<TapeResetEventArgs> TapeReset;
        event EventHandler<StepFinishedEventArgs> StepFinished;
        event EventHandler FinishedRunning;
    }
}
